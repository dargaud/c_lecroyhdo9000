///////////////////////////////////////////////////////////////////////////////
// This is a driver for the LeCroy HDO 9000 series, tested on the HDO9404
//
// See the README.md file for help
//
// Author: Guillaume Dargaud
// (c) GPL
///////////////////////////////////////////////////////////////////////////////

#include <ansi_c.h>
#include <iso646.h>

#include <cvirte.h>
#include <utility.h>
#include <visa.h>

#include "LecroyHdoVisa.h"

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Save a waveform buffer in a file
/// HIPAR	Pathname/Binary file to read from
/// HIPAR	Waveform/As obtained from LC9_GetWaveform()
/// HIPAR	Count/Size of waveform buffer
/// HIRET	0 if no error, errno otherwise
///////////////////////////////////////////////////////////////////////////////
int LC9_SaveWaveform(const char* PathName, const unsigned char* Waveform, int Count) {
	errno=0;
	if (Count<=0) return 0;
	FILE* fb=fopen(PathName, "wb");
	if (fb==NULL) { perror("Failed to save waveform"); return errno; }
	fwrite(Waveform, 1, Count, fb);
	fclose(fb);
	return errno;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read a waveform from a file
/// HIPAR	Pathname/Binary file to read from
/// HIPAR	Waveform/A pointer to an empty or allocated array. Will be reallocated
/// OUT		Count
/// HIPAR	Count/On exit: size of data in buf
/// HIRET	0 if no error, errno otherwise
///////////////////////////////////////////////////////////////////////////////
int LC9_ReadWaveform(const char* PathName, unsigned char** Waveform, int *Count) {
	int BOLE=SetBreakOnLibraryErrors(0);
	errno=0;
	FILE* fb=fopen(PathName, "rb");
	SetBreakOnLibraryErrors(BOLE);
	if (fb==NULL) { /*perror("Failed to read file");*/ *Count=0; return errno; }
	fseek(fb, 0, SEEK_END);
	*Count = ftell (fb);
	rewind(fb);
	*Waveform=realloc(*Waveform, *Count);
	if (*Waveform==NULL) { errno=ENOMEM; goto End; }
	memset(*Waveform, 0, *Count);	// Optional. In case the read is incomplete
	*Count=fread(*Waveform, 1, *Count, fb);
End:fclose(fb);
	return errno;
}

///////////////////////////////////////////////////////////////////////////////
/// HIPAR	Buf/Size should be at least 15.
/// HIPAR	HeaderSize/Number of bytes read into the buffer
/// HIRET	Number of bytes in rest of data to read, or VISA error if <0
/// HIRET	Total size of array should be at least HeaderSize+Ret+1
///////////////////////////////////////////////////////////////////////////////
static int GetPreHeaderSize(ViSession Instr, unsigned char* Buf, int *HeaderSize) {
	char Begin[16], SizeSize;
	errno=0;
	int Nb, St = viRead (Instr, Buf, 6, &Nb);
	if (St!=0 and St!=VI_SUCCESS_MAX_CNT) return St;	// No data ?
	if (Nb!=6) return errno=EINVAL;	// No data ?
	int N=sscanf((char*)Buf, "%[^,],#%c", Begin, &SizeSize);	// Begin=="ALL"
	if (N!=2) return errno=EINVAL;
	int SS=SizeSize-'0';	// Should be 9
	*HeaderSize=strlen(Begin)+3+SS;
	St = viRead (Instr, Buf+strlen(Begin)+3, SS, &Nb);
	if (St and St!=VI_SUCCESS_MAX_CNT) return St;
	if (Nb!=SS) return errno=EINVAL;
	Buf[*HeaderSize]=0;
	return atoi((char*)Buf+strlen(Begin)+3);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Decode a binary buffer obtained from the Lecroy into a more useful
/// HIFN	structure and a series of data pointers within that struct.
/// HIFN	Keep the Waveform buffer while using this (don't free or reuse it)
/// OUT		Content, Comment
/// HIPAR	Waveform/Buffer obtained from LecroyGetWaveform() or ReadWaveform()
/// HIPAR	Waveform/Keep this buffer around after the call while using Content (don't free or reuse it)
/// HIPAR	Count/Size of the buffer
/// HIPAR	Content/Series of pointers to the *inside* of Waveform
/// HIPAR	Content/Note: this function doesn't set ChanType/ChanNb
/// HIPAR	Comment/Optional string for returned info. Pass NULL is not wanted. Must be 'big enough'
///////////////////////////////////////////////////////////////////////////////
int LC9_DecodeWaveform(const unsigned char* Waveform, int Count, tLC9_WfContent *Content, char Comment[]) {
	#define Bail { fprintf(stderr, "\nUnrecognised waveform encoding\n"); return EINVAL/*EBADMSG*/; }
	const unsigned char *P=Waveform;
	tLC9_WaveDesc *WD=NULL;
	char C, W[20];					// Number of digits following (normally nine)
	int N, Nine=0, /*What=0,*/ WdSize=0;

	memset(Content, 0, sizeof(tLC9_WfContent));	// Clean up

	//N=sscanf((char*)P, "ALL,#%c%04d%05d", &C, &What, &WdSize);
	N=sscanf((char*)P, "ALL,#%c", &C);
	if (N!=1) Bail;
	Nine=C-'0';	// Is this related to DEF9 ?
	strncpy(W, (char*)(P+=6), Nine); W[Nine]=0;
	WdSize=atoi(W);

	if (Comment) sprintf(Comment, "\nALL,#%d %d\n", Nine, WdSize);
	if (Nine==0 or WdSize==0) Bail;
	//P+=strlen(Ref);
	P+=Nine;

	// If not we have the wrong structure - see template again
	if (Count-WdSize!=Nine+6+1) {	// One extra byte for some reason
		if (Comment) sprintf(Comment+strlen(Comment), "Size discrepancy: Count: %d, Struct: %d, WdSize: %d\n",
								 Count, sizeof(tLC9_WaveDesc), WdSize);
		Bail;
	}

	if (0==strcmp((char*)P, "WAVEDESC")) {
		Content->WD=WD=(tLC9_WaveDesc*)P;
		if (0!=strcmp(WD->DESCRIPTOR_NAME, "WAVEDESC")) {
			if (Comment) sprintf(Comment+strlen(Comment), "\nInvalid block start: %s", P);
			Bail;
		}
		Content->IsWord=&WD->COMM_TYPE;
		int ElemSize=(*(Content->IsWord)?sizeof(short):sizeof(char));

		if (Comment) sprintf(Comment+strlen(Comment), "%s, %s, CommType:%d, CommOrder:%d\n",
			   WD->DESCRIPTOR_NAME, WD->TEMPLATE_NAME, WD->COMM_TYPE, WD->COMM_ORDER);
		if (Comment) sprintf(Comment+strlen(Comment), "Block sizes: WAVE_DESCRIPTOR=%d, USER_TEXT=%d, RES_DESC1=%d\n",
			   WD->WAVE_DESCRIPTOR, WD->USER_TEXT, WD->RES_DESC1);
		if (Comment) sprintf(Comment+strlen(Comment), "Arrays sizes: TRIGTIME=%d, RIS_TIME=%d, WAVE1=%d, WAVE2=%d\n",
			   WD->TRIGTIME_ARRAY, WD->RIS_TIME_ARRAY, WD->WAVE_ARRAY_1, WD->WAVE_ARRAY_2);
		if (Comment) sprintf(Comment+strlen(Comment), "Instrument name/nb/label: %s, %d, %s\n",
			   WD->INSTRUMENT_NAME, WD->INSTRUMENT_NUMBER, WD->TRACE_LABEL);

		if (sizeof(tLC9_WaveDesc)!=WD->WAVE_DESCRIPTOR) {
			if (Comment) sprintf(Comment+strlen(Comment), "Size discrepancy: %d!=%d\n",
									 sizeof(tLC9_WaveDesc), WD->WAVE_DESCRIPTOR);
			Bail;
		}
		// Lots more to print in Comment if one so desires

		// Next block
		P+=WD->WAVE_DESCRIPTOR;

		// USERTEXT, now set the appropriate pointers
		if (WD->USER_TEXT)  Content->UserText  =(char*)P;
		P+= WD->USER_TEXT;	Content->UserTextNb=WD->USER_TEXT;

		// TRIGTIME
		if (WD->TRIGTIME_ARRAY) Content->TrigTimes=(tLC9_TrigTime*)P;
		P+= WD->TRIGTIME_ARRAY;	Content->TrigTimesNb=WD->TRIGTIME_ARRAY/sizeof(tLC9_TrigTime);

		// RISTIME
		if (WD->RIS_TIME_ARRAY) Content->RisTimes=(double*)P;
		P+= WD->RIS_TIME_ARRAY;	Content->RisTimesNb=WD->RIS_TIME_ARRAY/sizeof(double);

		P+=WD->RES_ARRAY1;

		// DATA_ARRAY_1 or SIMPLE.
		if (WD->WAVE_ARRAY_1!=WD->WAVE_ARRAY_COUNT*ElemSize) {
			if (Comment) sprintf(Comment+strlen(Comment), "Size discrepancy: ARRAY_1=%d != ARRAY_COUNT=%d (*%s)\n",
					WD->WAVE_ARRAY_1, WD->WAVE_ARRAY_COUNT, *(Content->IsWord)?"short":"byte");
			Bail;
		}
		if (WD->WAVE_ARRAY_1)	Content->Data1=P;
		P+= WD->WAVE_ARRAY_1;	Content->Data1Nb=WD->WAVE_ARRAY_1/ElemSize;

		// DATA_ARRAY_2
		if (WD->WAVE_ARRAY_2)	Content->Data2=P;
		P+= WD->WAVE_ARRAY_2;	Content->Data2Nb=WD->WAVE_ARRAY_2/ElemSize;

		P+=WD->RES_ARRAY2;
		P+=WD->RES_ARRAY3;
	}
	if (P-Waveform==Count-1)
		return 0;	// The normal end

	if (P-Waveform>=Count) {
		if (Comment) sprintf(Comment+strlen(Comment), "Unexpected EOF: %d!=%d\n",
								 P-Waveform, Count);
		Bail;
	}
	if (Comment) sprintf(Comment+strlen(Comment), "Incomplete waveform read: %d!=%d\n",
							 P-Waveform, Count);
	return 1;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Just call this before you want to free your waveform buffer (optional)
/// HIPAR	Content/As obtained from LC9_DecodeWaveform
///////////////////////////////////////////////////////////////////////////////
extern void LC9_ClearContent(tLC9_WfContent *Content) {
	memset(Content, 0, sizeof(tLC9_WfContent));
	// There are no dynamic allocations, so nothing else needs to be done
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Convert the Data1 array to voltage values, removing invalid points
/// HIFN	More precisely it converts to the Y axis unit (may be nb of EVENTs in the case of a histogram for instance)
/// HIPAR	Content/As obtained from LC9_DecodeWaveform
/// HIPAR	Data/0 or 1: convert Data1, 2: convert rarely used Data2 (FFT imaginary, extrema...)
/// HIPAR	Volts/Must be at least size Content->Data1Nb.
/// HIPAR	Volts/Note this is a float as it's enough for 8/10 bits of precision
/// OUT		Volts
/// HIRET	Effective number of data points in array (may be lower than Content->Data1Nb)
///////////////////////////////////////////////////////////////////////////////
int LC9_WaveformToVolts(const tLC9_WfContent *Content, int Data, float Volts[]) {
	tLC9_WaveDesc *WD=Content->WD;
	int Start=(WD->FIRST_VALID_PNT>=0 ? WD->FIRST_VALID_PNT : 0);
	int End  =(WD->LAST_VALID_PNT >=0 ? WD->LAST_VALID_PNT  : (Data==2?Content->Data2Nb:Content->Data1Nb));
	int Nb=End-Start+1;	// End is inclusive
	const void *D=(Data==2?Content->Data2:Content->Data1);
	if (*(Content->IsWord))
		 for (int i=0; i<Nb; i++)
			Volts[i]=((short*)D)[i+Start] * WD->VERTICAL_GAIN - WD->VERTICAL_OFFSET;
	else for (int i=0; i<Nb; i++)
			Volts[i]=(( char*)D)[i+Start] * WD->VERTICAL_GAIN - WD->VERTICAL_OFFSET;
	// TODO: Sequence Waveforms, see "Interpreting Vertical Data" in MAUI doc
	return Nb;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Computes the times from trigger from available info, removing invalid points
/// HIFN	More precisely it converts to the X axis unit (may be V in the case of a histogram for instance)
/// HIPAR	Content/As obtained from LC9_DecodeWaveform
/// HIPAR	Times/Must be at least size Content->Data1Nb. Will match Voltages.
/// OUT		Times
/// HIRET	Effective number of data points in array (may be lower than Content->Data1Nb)
///////////////////////////////////////////////////////////////////////////////
int LC9_WaveformToTimes(const tLC9_WfContent *Content, double Times[]) {
	tLC9_WaveDesc *WD=Content->WD;
	int Start=(WD->FIRST_VALID_PNT>=0 ? WD->FIRST_VALID_PNT : 0);
	int End  =(WD->LAST_VALID_PNT >=0 ? WD->LAST_VALID_PNT  : Content->Data1Nb);
	int Nb=End-Start+1;	// End is inclusive
	for (int i=0; i<Nb; i++)
		Times[i]=(i+Start) * WD->HORIZ_INTERVAL + WD->HORIZ_OFFSET;
	// TODO: Sequence Waveforms, see "Calculating the Horizontal Position of Data Points" in MAUI doc
	return Nb;
}



///////////////////////////////////////////////////////////////////////////////
/// HIFN	Init sequence for scope. Call only once per scope
/// HIFN	Some of the queries present here are not needed (only for info)
/// HIPAR	Resource/As obtained from viOpenDefaultRM
/// HIPAR	Descr/Instrument descriptor (VISA string or alias)
/// OUT		Comment, Instr
/// HIPAR	Comment/Optional string with returned info. Pass NULL is not wanted. Must be big enough
/// HIPAR	Instr/Instrument handle that will be returned by viOpen, to later pass to LC9_GetWaveform()
/// HIRET	0 if valid connection
///////////////////////////////////////////////////////////////////////////////
int LC9_InitConnection(ViSession Resource, const ViRsrc Descr, char Comment[], ViSession *Instr) {
	ViUInt32 Cnt, Val;
	ViStatus St;
	#define RMAX 1000			// According to specs, 256 should be enough
	char *S=NULL, Str[RMAX];	// Only for strings. Bigger replies go in a buffer
	#define CLR memset(Str, 0, RMAX)
	char Dum1[80], Dum2[80], data_t[80], SD[256];

	if (*Instr!=0) return 1;	// Already open instrument
	if (Comment) sprintf(Comment, "Opening: %s\n", Descr);

	int SBOLE=SetBreakOnLibraryErrors(0);
	St=viOpen (Resource, Descr, VI_NULL, 5, Instr);
	SetBreakOnLibraryErrors(SBOLE);
	if (*Instr==0 or St) {
Err:	viStatusDesc (Resource, St, SD);
		if (Comment) sprintf(Comment+strlen(Comment), "viOpen %s failed: %s", Descr, SD);
		return St;
	}

	// Deal with attributes
	unsigned short Attr=0;
	char Desc[256]="";
	viGetAttribute (*Instr, VI_ATTR_INTF_TYPE, &Attr);	// VI_INTF_TCPIP or VI_INTF_USB
	viGetAttribute (*Instr, VI_ATTR_INTF_INST_NAME, Desc);
	viSetAttribute (*Instr, VI_ATTR_SEND_END_EN, 1);	// Not necessary on USBTMC
	if (Comment) if (Comment) sprintf(Comment+strlen(Comment), "Interface: %s\n", Desc);

	viGetAttribute(*Instr, VI_ATTR_TMO_VALUE, &Val);
	if (Comment) if (Comment) sprintf(Comment+strlen(Comment), "Timeout: %dms\n", Val);
	viSetAttribute(*Instr, VI_ATTR_TMO_VALUE, 10000);
//	Default is 2000ms and is enough even for large buffers on USB3, but see COMM_FORMAT below
//	Note the the transfer time for even the largest buffers fits easily below 2s
//       BUT the repetition rate on the scope may not allow it to reply in time
//       In this case you should increase the timeout value

	// Scope basic commands
	St=viPrintf(*Instr, "COMM_HEADER OFF\n");			// Disallow echo
	if (St) goto Err;

	St=viQueryf(*Instr, S="*IDN?\n", "%s", Str);
	if (St) {
		viClear(*Instr);
		St=viPrintf(*Instr, "DCL\n");			// Device clear
		//St=viPrintf(*Instr, "SDC\n");			// Selected Device Clear
		//St=viPrintf(*Instr, "IFC\n");			// Interface Clear
		//St=viPrintf(*Instr, "*RST\n");			// Full-on Reset (will lose all config on scope)
//		CLR;
//		St=viQueryf(*Instr, S="CMR?\n", "%s", Str);		// Reads and clears the CoMmand error Register
//		if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
//		CLR;
//		St=viQueryf(*Instr, S="*ESR?\n", "%s", Str);	// Reads, clears the Event Status Register
//		if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
//		CLR;
//		St=viQueryf(*Instr, S="EXR?\n", "%s", Str);		// Reads, clears the EXecution error Register
//		if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
//		CLR;
//		St=viQueryf(*Instr, S="INR?\n", "%s", Str);		// Reads, clears Internal state change Register
//		if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
		Delay(1);
		CLR;
		St=viQueryf(*Instr, S="ALST?\n", "%s", Str);	// Reads and clears the contents of all status registers.
		if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
		if (St) goto Err;

		St=viQueryf(*Instr, S="*IDN?\n", "%s", Str);
	}	// After that we just assume things are working properly

	if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
	CLR;
	St=viQueryf(*Instr, S="*OPT?\n", "%s", Str);
	if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
	CLR;

	St=viPrintf(*Instr, "MESSAGE 'Connected to PC program'\n");

	St=viQueryf(*Instr, S="TIME_DIV?\n", "%s", Str);
	if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
	CLR;

	St=viQueryf(*Instr, S="COMM_ORDER?\n", "%s", Str);
	if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
	CLR;
	St=viQueryf(*Instr, S="COMM_FORMAT?\n", "%s", Str);
	if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
	sscanf(Str, "%[^,],%[^,],%[^,]", Dum1, data_t, Dum2);
	CLR;
	if (0==strcmp(data_t, "BYTE")) {
		// Note: if the ADC is 8 bits and you don't use averaging, transfer will go twice as fast if
		//       you leave it in BYTE, otherwise you'll gain some precision
		// Using USB2 instead of 3, with 200Mb buffers, the difference in speed is notable
		// and you may need to increase the timeout value above
		St=viPrintf(*Instr, S="COMM_FORMAT DEF9,WORD,BIN\n");
		St=viQueryf(*Instr, S="COMM_FORMAT?\n", "%s", Str);
		if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);
	}

	// Optional
	CLR;
	St=viQueryf(*Instr, S="CHL? CLR\n", "%s", Str);		// COMM_HEADER_LOG
	if (Comment) sprintf(Comment+strlen(Comment), "\n%s: %s\n", S, Str);

	// Template is optional - It was used to write the specs of tWaveDesc
	#define BST 100000
	static unsigned char Template[BST]={0};	// Because too large for stack
	CLR;
	St=viPrintf(*Instr, S="TEMPLATE?\n");
	St=viRead  (*Instr, Template, BST, &Cnt);
	if (Comment) sprintf(Comment+strlen(Comment), "\n%s: Template length %d\n", S, Cnt);
	LC9_SaveWaveform("Template.txt", Template, Cnt);
	for (unsigned int i=0; i<Cnt; i++) if (Template[i]==0) Template[i]='\n';
	Template[Cnt]=0;
	if (Comment) sprintf(Comment+strlen(Comment), "%s", (char*)Template);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Obtain a waveform buffer from the scope
/// HIPAR	ChanType/C/F/M/P/Z for Channel, Math, Measurement, Memory, Zoom buffers
/// HIPAR	Channel/1..4 for C (or 0 for default, will ignore ChanType then)
/// HIPAR	Channel/1..12 for F/M/P/Z...
/// HIPAR	Buf/Empty or already allocated buffer. Will contain the waveform. Must be freed by user or reused here
/// OUT		Count
/// HIPAR	Count/On exit: size of data in buf
/// HIPAR	Instr/Instrument handle returned by LC9_InitConnection
/// HIRET	0 if no error, may be VI_SUCCESS_MAX_CNT if buffer is not big enough, or other VI error code
///////////////////////////////////////////////////////////////////////////////
int LC9_GetWaveform(char ChanType, int Channel, unsigned char** Buf, int *Count, ViSession Instr) {
	int  HeaderSize=0, St=0, FirstTry=1;
	char *S;
	if (Count==NULL or Buf==NULL) return ENOMEM;
	if (Instr==0) {     *Count=0; return EBADF; }
//	St=viQueryf (Instr, S="WAVEDESC?\n", "%s", Str); printf("\n%s: %s\n", S, Str);

Retry:
	if (Channel==0) St=viPrintf(Instr, S="WAVEFORM?\n");
	else            St=viPrintf(Instr, S="%c%d:WAVEFORM?\n", ChanType, Channel);

	errno=*Count=0;
	if (*Buf==NULL) *Buf=realloc(*Buf, 64);	// Otherwise assume it's more than the min required 15 bytes
	// Get buffer size without reading the whole thing
	int Size=GetPreHeaderSize(Instr, *Buf, &HeaderSize);
	if (Size==VI_ERROR_TMO) { St=Size; goto TMO; }
	if (Size<0)  return Size;
	if (errno)   return errno;
	if (Size==0) return EBADF;
	*Buf=realloc(*Buf, Size+HeaderSize+1+1);	// +1 unexplained, +1 to make sure we go past the end
	if (*Buf==NULL) return errno=ENOMEM;
	St=viRead(Instr, *Buf+HeaderSize, Size+1+1 /*Maybe*/, Count);
	switch (St) {	// Note: it often fails for no reasons, and then communication's all screwed up
					// - retrying doesn't seem to help (will timeout after a few bytes only)
					// - issue STOP ?
					// - issue WAIT; *OPC ?
					// - *TRG ?
		case VI_SUCCESS:		break;	// Entire input was read: Size+2>Count
		case VI_SUCCESS_MAX_CNT:break;	// There may be data left in the buffer: Size+1==Count
TMO:	case VI_ERROR_TMO:	St=viClear(Instr); if (St) break;
							St=viPrintf(Instr, "COMM_HELP FD,YES\n");	// Activate help
							St=viPrintf(Instr, "DCL\n");			// Device clear
							if (FirstTry) { FirstTry=0; goto Retry; }
							break;
		default:break;					// Errors
	}
	*Count+=HeaderSize;
	//	printf("\n%s: Count %d\n", S, *Count);
	//	St=viQueryf(Instr, S="C1:INSPECT? \"WAVEDESC\"\n", "%s", Str); printf("\n%s: %s\n", S, Str);
	return St;
}



///////////////////////////////////////////////////////////////////////////////
/// HIFN	Close teh connection to the scope
/// HIPAR	Instr/Instrument handle returned by LC9_InitConnection
///////////////////////////////////////////////////////////////////////////////
void LC9_CloseConnection(ViSession Instr) {
	int St=0;
	if (Instr==0) return;
	St=viPrintf(Instr, "MESSAGE 'PC program stopped'\n");
	viClose(Instr); Instr=0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read the screen colors defined for the channels
/// HIPAR	ChanType/C, F or M
/// HIPAR	Colors/Array of 4 (C), or 12 (F or M) values
/// HIRET	Possible VISA error code
///////////////////////////////////////////////////////////////////////////////
int LC9_GetColors(char ChanType, int Colors[], ViSession Instr) {
	int St=0;
	int BOLE=SetBreakOnLibraryErrors(0);
	for (int i=0; i<(ChanType=='C'?4:12); i++) {
		if ((St=viQueryf(Instr, "%c%d:COLOR?\n", "%i", ChanType, i+1, &Colors[i])))
			break;
		Colors[i]=((Colors[i]>>16)&0xFF) | (Colors[i]&0xFF00) | ((Colors[i]&0xFF)<<16);
//		printf("%c%d 0x%06X\n", ChanType, i+1, Colors[i]);
	}
	SetBreakOnLibraryErrors(BOLE);
	return St;
}

///////////////////////////////////////////////////////////////////////////////
// TEST CASE -  This is for testing reading from file
#ifdef TEST_READ
#include <userint.h>
int main (int argc, char *argv[]) {
	#define BS 10000000
	unsigned char *WF=NULL;
	static char Comment[100000]="";	// static because may be too big for stack
	char Pathname[MAX_PATHNAME_LEN];
	int Count=0;
	tLC9_WfContent Content={NULL};

	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */

	// File with metadata and data
	puts("============================ C2 =========================================\n");
	Count=BS;
	if (VAL_EXISTING_FILE_SELECTED != FileSelectPopup ("", "*.trc", "*.trc", "Select trace file to open",
												   VAL_LOAD_BUTTON, 0, 0, 1, 0, Pathname))
		return 1;
	LC9_ReadWaveform(Pathname, &WF, &Count);
	if (Count) {
		LC9_DecodeWaveform(WF, Count, &Content, Comment);
		puts(Comment);

		// Convert to physical units
		float  *Volts = calloc (Content.Data1Nb, sizeof(float));	// Too large for direct [Content.Data1Nb] allocation on stack
		double *Times = calloc (Content.Data1Nb, sizeof(double));
		int NbV=LC9_WaveformToVolts(&Content, 1, Volts);
		int NbT=LC9_WaveformToTimes(&Content, Times);	// Normally Nb2==NB<=Data1.Nb
		XYGraphPopup ("Lecroy waveform", Times, Volts, NbT, VAL_DOUBLE, VAL_FLOAT);

		// Optional
		LC9_ClearContent(&Content);
		free(WF);    WF=NULL;	// Don't need to free if doing multiple LC9_ReadWaveform
		free(Times); Times=NULL;
		free(Volts); Volts=NULL;
	}
	return 0;
}
#endif

///////////////////////////////////////////////////////////////////////////////
// TEST CASE -  This is for testing acquisition from scope
#ifdef TEST_ACQ
int main (int argc, char *argv[]) {
	#define BS 10000000
	static unsigned char *WF=NULL;	// static because may be too big for stack
	static char Comment[100000]="";	// same
	int Count=0;
	tLC9_WfContent Content={NULL};
	ViSession Resource=0, Instr=0;

	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */

	viOpenDefaultRM (&Resource);
	int NOK=LC9_InitConnection(Resource, "MyScope", Comment, &Instr);
	if (NOK) { puts(Comment); return NOK; }
	LC9_GetWaveform('C', 0, &WF, &Count, Instr);
	LC9_SaveWaveform("WaveformC0.trc", WF, Count);
	if (Count) LC9_DecodeWaveform(WF, Count, &Content, Comment);
	puts(Comment);

	LC9_CloseConnection(Instr);
	viClose (Resource);
	return 0;
}
#endif
