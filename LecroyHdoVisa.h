#ifndef __LECROY_HDO_VISA_H
#define __LECROY_HDO_VISA_H

typedef struct __attribute__((__packed__)) sLC9_TimeStamp {
	double  seconds;			// 0 to 59
	unsigned char  minutes;		// 0 to 59
	unsigned char  hours;		// 0 to 23
	unsigned char  days;		// 1 to 31
	unsigned char  months;		// 1 to 12
	unsigned short year;		// 0 to 16000
	unsigned short unused;
} tLC9_TimeStamp;

// This is copied and adapted from the result of the TEMPLATE? command
typedef struct __attribute__((__packed__)) sLC9_WaveDesc {
	char DESCRIPTOR_NAME[16];	// the first 8 chars are always WAVEDESC
	char TEMPLATE_NAME[16];
	short COMM_TYPE;			// chosen by remote command COMM_FORMAT 0 byte, 1 word
	short COMM_ORDER;			// 0 HIFIRST (TODO: not implemented), 1 LOFIRST

	// The following variables of this basic wave descriptor block specify
	// the block lengths of all blocks of which the entire waveform (as it is
	// currently being read) is composed. If a block length is zero, this
	// block is (currently) not present.
	// Blocks and arrays that are present will be found in the same order
	// as their descriptions below.

	// BLOCKS :
	long WAVE_DESCRIPTOR;		// length in bytes of block WAVEDESC
	long USER_TEXT;				// length in bytes of block USERTEXT
	long RES_DESC1;

	// ARRAYS :
	long TRIGTIME_ARRAY;		// length in bytes of TRIGTIME array
	long RIS_TIME_ARRAY;		// length in bytes of RIS_TIME array
	long RES_ARRAY1;			// an expansion entry is reserved
	long WAVE_ARRAY_1;			// length in bytes of 1st simple data array.
								// In transmitted waveform, represent the number of transmitted
								// bytes in accordance with the NP parameter of the WFSU remote command
								// and the used format (see COMM_TYPE).
	long WAVE_ARRAY_2;			// length in bytes of 2nd simple data array
	long RES_ARRAY2;
	long RES_ARRAY3;			// 2 expansion entries are reserved

	// The following variables identify the instrument
	char INSTRUMENT_NAME[92-76];
	long INSTRUMENT_NUMBER;
	char TRACE_LABEL[112-96];	// identifies the waveform.
	short RESERVED1;
	short RESERVED2;			// 2 expansion entries

	// The following variables describe the waveform and the time at which the waveform was generated.
	long WAVE_ARRAY_COUNT;		// number of data points in the data array.
								// If there are two data arrays (FFT or Extrema),
								// this number applies to each array separately.
	long PNTS_PER_SCREEN;		// nominal number of data points on the screen
	long FIRST_VALID_PNT;		// count of number of points to skip before first good point
								// FIRST_VALID_POINT = 0 for normal waveforms.
	long LAST_VALID_PNT;		// index of last good data point in record before padding (blanking) was started.
								// LAST_VALID_POINT = WAVE_ARRAY_COUNT-1
								// except for aborted sequence and rollmode acquisitions
	long FIRST_POINT;			// for input and output, indicates the offset relative to the beginning of the trace buffer.
								// Value is the same as the FP parameter of the WFSU remote command.
	long SPARSING_FACTOR;		// for input and output, indicates the sparsing into the transmitted data block.
								// Value is the same as the SP parameter of the WFSU remote command.
	long SEGMENT_INDEX;			// for input and output, indicates the index of the transmitted segment.
								// Value is the same as the SN parameter of the WFSU remote command.
	long SUBARRAY_COUNT;		// for Sequence, acquired segment count, between 0 and NOM_SUBARRAY_COUNT
	long SWEEPS_PER_ACQ;		// for Average or Extrema, number of sweeps accumulated else 1
	short POINTS_PER_PAIR;		// for Peak Dectect waveforms (which always include data points in DATA_ARRAY_1 and
								// min/max pairs in DATA_ARRAY_2).
								// Value is the number of data points for each min/max pair.
	short PAIR_OFFSET;			// for Peak Dectect waveforms only Value is the number of data points by
								// which the first min/max pair in DATA_ARRAY_2 is offset relative to the
								// first data value in DATA_ARRAY_1.
	float VERTICAL_GAIN;
	float VERTICAL_OFFSET;		// to get floating values from raw data : VERTICAL_GAIN * data - VERTICAL_OFFSET
	float MAX_VALUE;			// maximum allowed value. It corresponds to the upper edge of the grid.
	float MIN_VALUE;			// minimum allowed value. It corresponds to the lower edge of the grid.
	short NOMINAL_BITS;			// a measure of the intrinsic precision of the observation:
								// ADC data is 8 bit, averaged data is 10-12 bit, etc.
	short NOM_SUBARRAY_COUNT;	// for Sequence, nominal segment count else 1
	float HORIZ_INTERVAL;		// sampling interval for time domain waveforms
	double HORIZ_OFFSET;		// trigger offset for the first sweep of the trigger,
								// seconds between the trigger and the first data point
	double PIXEL_OFFSET;		// needed to know how to display the waveform
	char VERTUNIT[244-196];		// units of the vertical axis
	char HORUNIT[292-244];		// units of the horizontal axis
	float HORIZ_UNCERTAINTY;	// uncertainty from one acquisition to the next, of the horizontal offset in seconds
	tLC9_TimeStamp TRIGGER_TIME;// time of the trigger
	float ACQ_DURATION;			// duration of the acquisition (in sec) in multi-trigger waveforms.
								// (e.g. sequence, RIS,  or averaging)
	short RECORD_TYPE;/*
		   _0      single_sweep
		   _1      interleaved
		   _2      histogram
		   _3      graph
		   _4      filter_coefficient
		   _5      complex
		   _6      extrema
		   _7      sequence_obsolete
		   _8      centered_RIS
		   _9      peak_detect */
	short PROCESSING_DONE;/*
		   _0       no_processing
		   _1       fir_filter
		   _2       interpolated
		   _3       sparsed
		   _4       autoscaled
		   _5       no_result
		   _6       rolling
		   _7       cumulative*/
	short RESERVED5;			// expansion entry
	short RIS_SWEEPS;			// for RIS, the number of sweeps else 1

	// The following variables describe the basic acquisition
	// conditions used when the waveform was acquired
	short TIMEBASE;	/*
		   _0    1_ps/div
		   _1    2_ps/div
		   _2    5_ps/div
		   _3    10_ps/div
		   _4    20_ps/div
		   _5    50_ps/div
		   _6    100_ps/div
		   _7    200_ps/div
		   _8    500_ps/div
		   _9    1_ns/div
		   _10   2_ns/div
		   _11   5_ns/div
		   _12   10_ns/div
		   _13   20_ns/div
		   _14   50_ns/div
		   _15   100_ns/div
		   _16   200_ns/div
		   _17   500_ns/div
		   _18   1_us/div
		   _19   2_us/div
		   _20   5_us/div
		   _21   10_us/div
		   _22   20_us/div
		   _23   50_us/div
		   _24   100_us/div
		   _25   200_us/div
		   _26   500_us/div
		   _27   1_ms/div
		   _28   2_ms/div
		   _29   5_ms/div
		   _30   10_ms/div
		   _31   20_ms/div
		   _32   50_ms/div
		   _33   100_ms/div
		   _34   200_ms/div
		   _35   500_ms/div
		   _36   1_s/div
		   _37   2_s/div
		   _38   5_s/div
		   _39   10_s/div
		   _40   20_s/div
		   _41   50_s/div
		   _42   100_s/div
		   _43   200_s/div
		   _44   500_s/div
		   _45   1_ks/div
		   _46   2_ks/div
		   _47   5_ks/div
		   _100  EXTERNAL */
	short VERT_COUPLING;/*
		   _0      DC_50_Ohms
		   _1      ground
		   _2      DC_1MOhm
		   _3      ground
		   _4      AC_1MOhm */
	float PROBE_ATT;
	short FIXED_VERT_GAIN;/*
		   _0   1_uV/div
		   _1   2_uV/div
		   _2   5_uV/div
		   _3   10_uV/div
		   _4   20_uV/div
		   _5   50_uV/div
		   _6   100_uV/div
		   _7   200_uV/div
		   _8   500_uV/div
		   _9   1_mV/div
		   _10  2_mV/div
		   _11  5_mV/div
		   _12  10_mV/div
		   _13  20_mV/div
		   _14  50_mV/div
		   _15  100_mV/div
		   _16  200_mV/div
		   _17  500_mV/div
		   _18  1_V/div
		   _19  2_V/div
		   _20  5_V/div
		   _21  10_V/div
		   _22  20_V/div
		   _23  50_V/div
		   _24  100_V/div
		   _25  200_V/div
		   _26  500_V/div
		   _27  1_kV/div */
	short BANDWIDTH_LIMIT;		// _0 off, _1 on
	float VERTICAL_VERNIER;
	float ACQ_VERT_OFFSET;
	short WAVE_SOURCE;/*
		   _0      CHANNEL_1
		   _1      CHANNEL_2
		   _2      CHANNEL_3
		   _3      CHANNEL_4
		   _9      UNKNOWN	*/
} tLC9_WaveDesc;

typedef struct sLC9_TrigTime {	// TODO: NOT IMPLEMENTED - See "Interpreting Vertical Data" in "MAUI" documentation
	double TRIGGER_TIME;	// for sequence acquisitions, time in seconds from first trigger to this one
	double TRIGGER_OFFSET;	//  the trigger offset is in seconds from trigger to zeroth data point
} tLC9_TrigTime;

typedef struct sLC9_WfContent {
	tLC9_WaveDesc *WD;
	char *UserText;				int UserTextNb;
	tLC9_TrigTime *TrigTimes;	int TrigTimesNb;
	double *RisTimes;			int RisTimesNb;	// TODO: not implemented
	const void *Data1,  *Data2;	// Arrays of Short or Byte depending on *IsWord
	int   Data1Nb, Data2Nb;	// Number of samples in the arrays
	short *IsWord;			// Indicates Word or Byte in the (short/char*)Data1/2
} tLC9_WfContent;

///////////////////////////////////////////////////////////////////////////////

// Instrument communication
extern int  LC9_InitConnection(ViSession Resource, const ViRsrc Descr, char Comment[], ViSession *Instr);
extern int  LC9_GetWaveform(char ChanType, int Channel, unsigned char** Waveform, int *Count, ViSession Instr);
extern void LC9_CloseConnection(ViSession Instr);
extern int  LC9_GetColors(char ChanType, int Colors[], ViSession Instr);

// Buffer conversions and manipulation
extern int  LC9_SaveWaveform(const char* PathName, const unsigned char* Buf, int Count);
extern int  LC9_ReadWaveform(const char* PathName, unsigned char** Waveform, int *Count);
extern int  LC9_DecodeWaveform(const unsigned char* Waveform, int Count, tLC9_WfContent *Content, char Comment[]);
extern int  LC9_WaveformToVolts(const tLC9_WfContent *Content, int Data, float Volts[]);
extern int  LC9_WaveformToTimes(const tLC9_WfContent *Content, double Times[]);
extern void LC9_ClearContent(tLC9_WfContent *Content);

#endif
