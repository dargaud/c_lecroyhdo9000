# C_LecroyHDO9000

This is a VISA driver for the LeCroy HDO 9000 series, tested on the HDO9404, written in C for LabWindows/CVI

## General info
- **This driver is only meant to recover data from the scope, not to control the scope**
- It requires **VISA** and has been tested on Windows (compiled with LabWindows/CVI) but it could also work on Linux if you manage to install `ni-visa`.
- It works both in **USBTMC** and **Ethernet/LXI/VXI11** mode. You may need to install Vicp passport for the Ethernet transport: https://teledynelecroy.com/doc/understanding-vicp-and-the-vicp-passport
- It does not work in Ethernet/VICP mode (not sure what that would bring to the table anyway)
- It should work with multiple scopes at the same time but is not multithread-safe
- It does only one channel at the time, but you can call it for each channel in succession. *Important: If you want their timings to match, you should operate in [Single] or [Stop] trigger*
- You can acquire straight time domain waveform, but also other data formats (Math is tested, but not the others such as histogram, FFT...)
- Buffers are allocated dynamically and must be reused by the subsequent calls or freed by the user
- Ability to read/save buffers to files
- Conversions to voltages/times

## Test cases
There are 2 test cases at the bottom (use `/DTEST_READ` or `/DTEST_ACQ`)

## General principle:
- Start Visa with `viOpenDefaultRM()` then call `InitConnection()` to get an instrument handle
- Call `GetWaveform()` with a free buffer pointer, for each channel, as many times as you want
- Call `DecodeWaveform()` with that buffer and it returns a `struct sWfContent` with appropriate usable pointers. **Keep the original buffer** while using the struct (see header file).
- You can save/read that buffer and call `DecodeWaveform()` on reading it again
- Possibly call int `WaveformToTimes/Volts()` for physical units
- Repeat or close the connection

## Oscilloscope configuration

The Lecroy has 3 connection modes:
- USBTMC via USB (simple, no installation necessary). It shows in NI-MAX as `My Systems/Devices and Interfaces/Network Devices/USB0::0x05FF::0x1023::4404N30199::INSTR`. It needs to be set in [Utilities][[Utilities Setup][Remote] to [USBTMC]
- VICP via ethernet. Don't use it. It should show in NI-MAX as `VICP::192.168.0.233` but I haven't been able to get it to work.
- LXI/VXI11 via ethernet. You may need to install the VICP passport (not sure). You need to configure a 2nd ethernet adapter with a manual IP of 192.168.0.230/255.255.255.0, a crossover cable, also configure the scope [Utilities][[Utilities Setup][Remote] to [LXI (VXI11)] (not [TCPIP (VICP)]). The default address is 192.168.0.233 and is pingable. You can also have basic info at http://192.168.0.233/ . It shows in NI-MAX as  `My Systems/Devices and Interfaces/Network Devices/TCPIP0::192.168.0.233::inst0::INSTR`
- There's also GPIB and LSIB but I haven't tested them
- In all cases you should be able to open a VISA test panel and send a "*IDN?\n" query and obtain a reply with the identifier
- You can use an alias in NI-MAX as an instrument handle when calling InitConnection()

## Note 1
The structure returned by the scope (`sWaveDesc`) is defined based on the info returned from `TEMPLATE?` and may change between models or even between firmware versions of a single model, so this code may require adjustements
A perfect version would need to parse `TEMPLATE?` but it'd be too much of a waste of time, to say nothing of the complication

## Note 2
It would be nice to write an IVI driver from that. I won't.
It would also be nice to integrate it to the open-source scopehal-apps which doesn't yet support the HDO 9000 series. Go for it.
